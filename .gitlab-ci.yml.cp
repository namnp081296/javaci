image: maven:3-jdk-8

stages:
  - build
  - test
  - deploy

maven-build:
  stage: build
  script: 
    - mvn package
    - cat target/site/jacoco/index.html
  artifacts:
    expose_as: 'code coverage'
    paths:
      - target/surefire-reports/
      - target/site/jacoco/index.html
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml

coverage_check:
  image: alpine:latest
  stage: test
  script:
    - apk add bash
    - apk add jq
    - apk add curl
#    - bash code-coverage.sh

pages:
  stage: deploy
  dependencies:
    - maven-build
  script:
    - mv target/site/jacoco/ public/
  artifacts:
    paths:
      - public
    expire_in: 30 days
  only:
    - master
